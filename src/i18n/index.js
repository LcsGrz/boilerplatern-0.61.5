// LIBS
import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize'; // Use for caching/memoize for better performance
// TRADUCTIONS
import es from './translations/es.json';
import en from './translations/en.json';

const translationGetters = {
  es,
  en,
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);

const setI18nConfig = () => {
  // fallback if no available language fits
  const fallback = { languageTag: 'es', isRTL: false };

  const { languageTag } =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;

  // clear translation cache
  translate.cache.clear();

  // set i18n-js config
  i18n.translations = { [languageTag]: translationGetters[languageTag] };
  i18n.locale = languageTag;
};

export { setI18nConfig, translate as T };

// INFO
// https://dev.to/vikrantnegi/creating-a-multi-language-app-in-react-native-1joj
