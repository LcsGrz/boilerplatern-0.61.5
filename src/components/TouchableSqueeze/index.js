// REACT
import React, { Component } from 'react';
import { Animated, Easing, View, TouchableWithoutFeedback } from 'react-native';
// LIBS
import PropTypes from 'prop-types';
// UTILS
import { isIos } from '../../utils/commonUtils';

// --- SEPARATOR ---

class TouchableSqueeze extends Component {
  SqueezeValue = new Animated.Value(0);

  animateIn = () => {
    this.SqueezeValue.setValue(0);
    Animated.timing(this.SqueezeValue, {
      duration: isIos ? 450 : 900,
      easing: Easing.linear,
      toValue: 1,
      useNativeDriver: true,
    }).start();
  };

  animateOut = () => {
    this.SqueezeValue.setValue(0);
    Animated.timing(this.SqueezeValue, {
      duration: isIos ? 450 : 900,
      easing: Easing.linear,
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  scaleAndGoTo = () => {
    const { onPress } = this.props;
    this.animateOut();
    onPress();
  };

  render() {
    const { children, disabled, touchableStyle, viewStyle } = this.props;
    const squeezee = this.SqueezeValue.interpolate({
      inputRange: [0, 0.3, 0.6, 1],
      outputRange: [1, 0.99, 0.98, 0.96],
    });

    return (
      <Animated.View style={[{ transform: [{ scale: squeezee }] }, viewStyle]}>
        <TouchableWithoutFeedback
          disabled={disabled}
          onPress={this.scaleAndGoTo}
          onPressIn={this.animateIn}
          onPressOut={this.animateOut}>
          <View style={touchableStyle}>{children}</View>
        </TouchableWithoutFeedback>
      </Animated.View>
    );
  }
}

TouchableSqueeze.propTypes = {
  children: PropTypes.element.isRequired,
  disabled: PropTypes.bool,
  onPress: PropTypes.func,
  touchableStyle: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  ),
  viewStyle: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  ),
};

TouchableSqueeze.defaultProps = {
  disabled: false,
  onPress: () => {},
  touchableStyle: {},
  viewStyle: {},
};

export default TouchableSqueeze;
