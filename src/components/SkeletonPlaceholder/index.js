/* eslint-disable react/no-array-index-key */
// REACT
import * as React from 'react';
import { Animated, View, StyleSheet, Easing } from 'react-native';
// LIBS
import PropTypes from 'prop-types';
// COMPONENTS
import LinearGradient from 'react-native-linear-gradient';

// --- SEPARATOR ---

export default function SkeletonPlaceholder({
  children,
  backgroundColor,
  speed,
  highlightColor,
}) {
  const animatedValue = new Animated.Value(0);

  React.useEffect(() => {
    Animated.loop(
      Animated.timing(animatedValue, {
        toValue: 1,
        duration: speed,
        easing: Easing.ease,
      }),
    ).start();
  }, [animatedValue, speed]);

  const translateX = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [-350, 350],
  });

  const getChildren = element => {
    return React.Children.map(element, (child, index) => {
      const { style } = child.props;

      if (child.props.children) {
        return (
          <View key={index} style={style}>
            {getChildren(child.props.children)}
          </View>
        );
      }

      return (
        <View key={index} style={{ position: 'relative' }}>
          <View style={[style, { backgroundColor, overflow: 'hidden' }]}>
            <Animated.View
              style={[
                StyleSheet.absoluteFill,
                {
                  transform: [{ translateX }],
                },
              ]}>
              <LinearGradient
                colors={[backgroundColor, highlightColor, backgroundColor]}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={{ flex: 1 }}
              />
            </Animated.View>
          </View>
        </View>
      );
    });
  };

  return <>{getChildren(children)}</>;
}

SkeletonPlaceholder.propTypes = {
  children: PropTypes.element.isRequired,
  backgroundColor: PropTypes.string,
  highlightColor: PropTypes.string,
  speed: PropTypes.number,
};

SkeletonPlaceholder.defaultProps = {
  backgroundColor: '#E1E9EE',
  highlightColor: '#F2F8FC',
  speed: 800,
};
