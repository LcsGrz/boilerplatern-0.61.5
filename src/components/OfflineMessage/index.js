// REACT
import React, { Component } from 'react';
import { Animated, SafeAreaView, View, StatusBar } from 'react-native';
// LIBS
import NetInfo from '@react-native-community/netinfo';
import PropTypes from 'prop-types';
// COMPONENTS
import Text from '../Text';
// ASSETS
import { NoInternet } from '../../assets/svg';
// STYLES
import styles from './styles';
import colors from '../../theme/colors';
// UTILS
import { T } from '../../i18n';
import { RS } from '../../utils/dimensions';

// --- SEPARATOR ---

const ANIMATION_TIME = 500;

class OfflineMessage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isConnected: true,
      animation: new Animated.Value(0),
    };
  }

  componentDidMount() {
    this.unsubscribe = NetInfo.addEventListener(this.handleConnectivityChange);
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  handleConnectivityChange = ({ isConnected }) =>
    this.onAlternateView(isConnected);

  unsubscribe = () => {};

  onAlternateView = async isConnected => {
    const { insets } = this.props;
    const { animation } = this.state;

    const distance = isConnected ? 0 : RS(48) + 50 + insets.top;

    if (!isConnected) this.setState({ isConnected: false });

    Animated.timing(animation, {
      toValue: distance,
      duration: ANIMATION_TIME,
      useNativeDriver: false,
    }).start(() => this.setState({ isConnected }));
  };

  render() {
    const { isConnected, animation } = this.state;

    if (isConnected) return null;

    return (
      <View style={styles.offlineMessage}>
        <Animated.View style={[styles.container, { height: animation }]}>
          <StatusBar
            backgroundColor={colors.brightAqua}
            barStyle="light-content"
          />
          <SafeAreaView style={styles.full}>
            <View style={styles.box}>
              <View style={styles.svg}>
                <NoInternet height="100%" width="100%" />
              </View>
              <Text size="sms" color="white" style={styles.textFix}>
                {`${T('offlineMessage.ouch')} `}
                <Text size="sms" color="white" type="bold">
                  {T('offlineMessage.check')}
                </Text>
              </Text>
            </View>
          </SafeAreaView>
        </Animated.View>
      </View>
    );
  }
}

OfflineMessage.propTypes = {
  insets: PropTypes.objectOf(PropTypes.number).isRequired,
};

export default OfflineMessage;
