// REACT
import { StyleSheet } from 'react-native';
// UTILS
import { RS } from '../../utils/dimensions';
// STYLES
import colors from '../../theme/colors';

export default StyleSheet.create({
  offlineMessage: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    position: 'absolute',
    width: '100%',
  },
  container: {
    backgroundColor: colors.brightAqua,
    width: '100%',
    borderBottomEndRadius: RS(12),
    borderBottomLeftRadius: RS(12),
    overflow: 'hidden',

    elevation: 4,
  },
  full: {
    flex: 1,
  },
  box: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: RS(24),
    justifyContent: 'center',
    alignItems: 'center',
  },
  svg: {
    height: RS(23),
    width: RS(30),
    marginRight: RS(15),
  },
  textFix: {
    marginTop: RS(5),
  },
});
