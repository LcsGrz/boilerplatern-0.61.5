// REACT
import React, { PureComponent } from 'react';
import { Image, ImageBackground } from 'react-native';
// LIBS
import { PropTypes } from 'prop-types';
// UTILS
import { RS } from '../../utils/dimensions';

// --- SEPARATOR ---

class ImagePlaceholder extends PureComponent {
  render() {
    const {
      placeholderImage,
      realImage,
      resizeMode,
      height,
      width,
      radius,
      style,
    } = this.props;

    const imageStyle = {
      height: RS(height),
      width: RS(width),
      borderRadius: RS(radius),
    };

    return (
      <ImageBackground
        source={placeholderImage}
        resizeMode={resizeMode}
        style={[imageStyle, style]}>
        <Image
          source={realImage ? { uri: realImage } : null}
          resizeMode={resizeMode}
          style={imageStyle}
        />
      </ImageBackground>
    );
  }
}

ImagePlaceholder.propTypes = {
  placeholderImage: PropTypes.number.isRequired,
  realImage: PropTypes.string.isRequired,
  resizeMode: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  radius: PropTypes.number,
  style: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  ),
};

ImagePlaceholder.defaultProps = {
  radius: 0,
  style: {},
};

export default ImagePlaceholder;
