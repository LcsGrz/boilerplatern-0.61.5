// REACT
import React, { PureComponent } from 'react';
import { TouchableOpacity, Text } from 'react-native';
// LIBS
import PropTypes from 'prop-types';
// UTILS
import { T as translator } from '../../i18n';
import { normalize } from '../../utils/dimensions';
// STYLES
import colors from '../../theme/colors';
import fonts from '../../assets/fonts';

// --- SEPARATOR ---

class TouchableTypography extends PureComponent {
  textProps = () => {
    const { textSize, type, color, textAlign } = this.props;

    return {
      fontSize: normalize(textSize),
      fontFamily: fonts[type],
      color: colors[color] || color,
      textAlign,
    };
  };

  render() {
    const { onPress, touchableStyle, textStyle, T, children } = this.props;

    if (!T && !children) return null;

    return (
      <TouchableOpacity
        disabled={!onPress}
        onPress={onPress}
        style={touchableStyle}>
        <Text style={[this.textProps(), textStyle]}>
          {T ? translator(T) : children}
        </Text>
      </TouchableOpacity>
    );
  }
}

TouchableTypography.propTypes = {
  onPress: PropTypes.func,
  touchableStyle: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  ),
  textStyle: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  ),
  textSize: PropTypes.number,
  T: PropTypes.string,
  color: PropTypes.string,
  textAlign: PropTypes.string,
  children: PropTypes.oneOf([PropTypes.string, PropTypes.element]),
  type: PropTypes.oneOf(Object.keys(fonts)),
};

TouchableTypography.defaultProps = {
  onPress: undefined,
  touchableStyle: {},
  textStyle: {},
  textSize: 12,
  T: undefined,
  children: undefined,
  color: 'black',
  textAlign: 'left',
  type: fonts.regular,
};

export default TouchableTypography;
