import { Dimensions } from 'react-native';

const windowDimensions = Dimensions.get('window');

export const REFERENCE_WIDTH = 375;
export const REFERENCE_HEIGHT = 667;

export const WINDOW_HEIGHT = windowDimensions.height;
export const WINDOW_WIDTH = windowDimensions.width;

const USE_WIDTH =
  WINDOW_WIDTH / REFERENCE_WIDTH < WINDOW_HEIGHT / REFERENCE_HEIGHT;

export const REF_RATIO = USE_WIDTH
  ? WINDOW_WIDTH / REFERENCE_WIDTH
  : WINDOW_HEIGHT / REFERENCE_HEIGHT;

export const SCALE = WINDOW_WIDTH / 320; // based on iphone 5s's scale
