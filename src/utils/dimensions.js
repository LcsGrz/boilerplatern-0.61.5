import { Dimensions, Platform, PixelRatio } from 'react-native';
import { REF_RATIO, SCALE } from './responsive';

const { height, width } = Dimensions.get('window');

export const H = height;
export const W = width;

export function normalize(size) {
  const newSize = size * SCALE;

  if (Platform.OS === 'ios')
    return Math.round(PixelRatio.roundToNearestPixel(newSize));

  return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
}

export function RS(value) {
  return Math.round(value * REF_RATIO);
}

export function RSPlatForm(iphoneSize, androidSize) {
  return Platform.OS === 'ios' ? RS(iphoneSize) : RS(androidSize);
}
