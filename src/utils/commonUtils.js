// REACT
import { Platform } from 'react-native';
// UTILS
import { RS } from './dimensions';

// --- SEPARATOR ---

// --- FUNCTIONS ---

export const randomBool = () => Math.random() >= 0.5;

export const randomNumber = (min, max) =>
  Math.round(min + Math.random() * (max - min));

export const rsRandomNumber = (min, max) => RS(randomNumber(min, max));

export const numberArray = n => [...new Array(n)].map((el, i) => `${1 + i}`);

// --- CONST ---

export const isAndroid = Platform.OS === 'android';
export const isIos = Platform.OS === 'ios';
