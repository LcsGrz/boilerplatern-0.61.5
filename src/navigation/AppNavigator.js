// REACT NATIVE
import React, { PureComponent } from 'react';
// NAVIGATION
import { NavigationContainer } from '@react-navigation/native';
// import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import { createStackNavigator } from '@react-navigation/stack';
// CONST
import ROUTES from './routes';
// SCREENS
import App from '../scenes/App';

const Stack = createStackNavigator();

export default class AppNavigator extends PureComponent {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name={ROUTES.app} component={App} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
