// import { NavigationActions, StackActions } from 'react-navigation';
import ROUTES from './routes';
import AppNavigator from './AppNavigator';

// let navigator;

// function setTopLevelNavigator(navigatorRef) {
//   navigator = navigatorRef;
// }

// function getTopLevelNavigator() {
//   return navigator;
// }

// function navigate(routeName, params = {}) {
//   navigator.dispatch(
//     NavigationActions.navigate({
//       routeName,
//       params,
//     }),
//   );
// }

// function push(routeName, params = {}) {
//   navigator.dispatch(
//     StackActions.push({
//       routeName,
//       params,
//     }),
//   );
// }

// function goBack(key) {
//   navigator.dispatch(NavigationActions.back({ key }));
// }

// function resetStack(routeName, params = {}) {
//   navigator.dispatch(
//     StackActions.reset({
//       index: 0,
//       actions: [
//         NavigationActions.navigate({
//           routeName,
//           params,
//         }),
//       ],
//     }),
//   );
// }

// function getActiveRouteParams(navigationState = navigator.state.nav) {
//   const route = navigationState.routes[navigationState.index];
//   return route.routes ? getActiveRouteParams(route) : route.params;
// }

// function getActiveRouteName(navigationState = navigator.state.nav) {
//   const route = navigationState.routes[navigationState.index];
//   return route.routes ? getActiveRouteName(route) : route.routeName;
// }

export default {
  // setTopLevelNavigator,
  // getTopLevelNavigator,
  // navigate,
  // push,
  // goBack,
  // resetStack,
  // getActiveRouteParams,
  // getActiveRouteName,
  AppNavigator,
  ROUTES,
};
