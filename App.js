// REACT NATIVE
import React, { Component } from 'react';
import { StatusBar } from 'react-native';
// NAVIGATION
import 'react-native-gesture-handler';
import N from './src/navigation';
// LIBS
import SplashScreen from 'react-native-splash-screen';
import {
  SafeAreaProvider,
  SafeAreaConsumer,
} from 'react-native-safe-area-context';
import * as RNLocalize from 'react-native-localize';
// I18N
import { setI18nConfig } from './src/i18n';
// COMPONENTS
import { OfflineMessage } from 'Components';

class App extends Component {
  constructor(props) {
    super(props);
    setI18nConfig(); // set initial config
  }

  async componentDidMount() {
    SplashScreen.hide();
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };

  render() {
    return (
      <>
        <SafeAreaProvider>
          <StatusBar barStyle="dark-content" backgroundColor="transparent" />
          <N.AppNavigator
          // ref={navigatorRef => N.setTopLevelNavigator(navigatorRef)}
          />
          <SafeAreaConsumer>
            {insets => <OfflineMessage insets={insets} />}
          </SafeAreaConsumer>
        </SafeAreaProvider>
      </>
    );
  }
}

export default App;
